package AB01;
import java.util.Random;
import java.util.Scanner;

// P Anfang

/**
 * 
 * Aufgabe7_Lotto
 * Das Programm verlangt vom Nutzer 6 Zahlen zwischen 1 und 49.
 * Danach werden 6 Zufallszahlen generiert und mit den Zahlen vom Nutzer abgeglichen.
 * Anschlie�end werden die richtigen Treffer ausgegeben.
 * Das Programm l�sst keine doppelten sowie ung�ltige Eingaben zu.
 * 
 * @version 1.0 vom 30.01.2020
 * @author matthias.amelung
 * FS94
 * 
 */
public class Aufgabe7_Lotto {

	// HP Anfang
	public static void main(String[] args) {

		// D Anfang
		Scanner sc = new Scanner(System.in);
		Random zufall = new Random();
		int tipp[] = new int[6];	// getippte Zahlen
		int lotto[] = new int[6];	// Lottozahlen
		int richtigTipp = 0;		// Anzahl richtiger Tipps
		boolean flag = true;		// Control-Flag ob eine Zahl im Array schon existiert
		// D Ende

		do {
			
			// Schleife f�r das Lotto-Array
			for (int i = 0; i < lotto.length; i++) {			// f�llt das lotto-Array
				do {											// Pr�fung, ob die Zahl schon im Array ist
					lotto[i] = 1 + zufall.nextInt(49);			// Zahl ins Array eintragen
					for (int j = 0; j < lotto.length; j++) {	// geht durch das Array durch
						if (lotto[i] != lotto[j] || i == j) {	// Eingabe korrekt, wenn i = j ist oder die Werte an den Indizes ungleich
							flag = false;
						} else {
							j = lotto.length-1;					// bricht die Schleife ab
							flag = true;						// flag auf true, Duplikat gefunden
						}
					}
				} while (flag);
			}
			
			flag = true;	// zur�cksetzen von flag

			System.out.println("Bitte nur Tipps zwischen 1 und 49 eingeben und keine Duplikate.");
			for (int i = 0; i < tipp.length; i++) {					// f�llt das tipp-Array
				do {												// Pr�fung, ob Zahl schon im Array ist
					do {											// Eingabepr�fung (Nur Tipps zwischen 1 und 49)
						System.out.print("Geben Sie ihren " + (i+1) + ". Tipp ein: ");
						tipp[i] = sc.nextInt();
					} while (tipp[i] < 1 || tipp[i] > 49);
					
					for (int j = 0; j < tipp.length; j++) {			// geht durch das Array durch)
						if (tipp[i] != tipp[j] || i == j) {			// Eingabe wird genommen, wenn i = j ist oder Werte an den Indizes ungleich
							flag = false;
						} else {
							j = tipp.length-1;						// bricht Schleife ab
							flag = true;							// Duplikat gefunden
						}
					}
				} while (flag);
			}
			
			for (int i = 0; i < lotto.length; i++) {				// W�hlt Zahl im lotto-Array mit Index i
				for (int j = 0; j < tipp.length; j++) {				// Schleife zum Test der Zahlen, geht durch JEDE tipp-Zahl durch
					if (tipp[j] == lotto [i]) {						// Wenn eine Zahl aus tipp mit der Zahl aus lotto des Index i gleich ist
						richtigTipp++;								// richtigTipp um 1 erh�hen
					}
				}
			}

			System.out.println("Es gibt " + richtigTipp + " richtige.\n");	// Ausgabe richtiger Tipps
			System.out.print("Nochmal? (j) ");								// Frage nach Wiederholung
		} while (sc.next(".").toLowerCase().charAt(0) == 'j');
		
		// Scanner schlie�en
		sc.close();
		
	}
	// HP Ende
}
// P Ende