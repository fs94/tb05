/**
 * 
 * Lagerverwaltung
 * Das Programm erm�glicht die Verwaltung eines Lagersystems
 * Jedes Fach besitzt eine maximale Artikelanzahl von 1000 Artikeln, Bezeichnung und Preis
 * Erm�glicht es dem Nutzer alle Daten im Lager abzufragen, bestimmte Daten abzufragen,
 * alle Daten neu einzugeben, bestimmte Daten zu �ndern und Artikel zu suchen.
 * Lagergr��e kann mit ANZ_RR, ANZ_R und ANZ_F bestimmt werden. 
 * 
 * 
 * @version 1.0 vom 19.02.2020
 * @author matthias.amelung
 * FS94
 * 
 */
	
package AB02;

import java.text.DecimalFormat;
import java.util.Scanner;

// P Anfang
public class Aufgabe2_Lager {
	
	// HP Anfang
	public static void main(String[] args) {
		// D Anfang
		Scanner sc = new Scanner(System.in);
		DecimalFormat f = new DecimalFormat("#.##");
		
		final int ANZ_RR = 1;		// Regalreihen
		final int ANZ_R = 1;		// Regale in Reihe
		final int ANZ_F = 2; 		// Faecher in Regal
		final int ANZ_E = 3;		// Anzahl Eintraege im Fach
		String[][][][] lager = new String[ANZ_RR][ANZ_R][ANZ_F][ANZ_E];	// Lager
		String search;				// Datensuche-Parameter
		String eingabe;				// einfacher Eingabeparameter
		int wahl;					// Switch Variable
		int count = 0;				// Anzahl der Eintr�ge
		int size = ANZ_RR * ANZ_R * ANZ_F;	// Gr��e des Lagers
		int tmpInt = 0;				// tempor�re Variable f�r Berechnungen und Eingaben
		double tmpDbl = 0;			// tempor�re Variable f�r Berechnungen und Eingaben
		boolean repeat = true;		// Programm-Wdh-Varibale
		boolean found = false;		// Datensuche-Boolean
		// D Ende
		
		lager[0][0][0][0] = "Tomate";
		lager[0][0][0][1] = "500";
		lager[0][0][0][2] = "1,00 �";
		lager[0][0][1][0] = "Apfel";
		lager[0][0][1][1] = "233";
		lager[0][0][1][2] = "2,00 �";
		
		
		// Beginn Programm-Wdh
		do {
			// Z�hlt Eintr�ge im Lager
			count = 0;
			for (int row = 0; row < lager.length; row++) {
				for (int col = 0; col < lager[row].length; col++) {
					for (int field = 0; field < lager[row][col].length; field++) {
						try {
							// Wenn Lagereintr�ge im field nicht leer sind, wird count erh�ht
							if (!(lager[row][col][field][0] == null) && !(lager[row][col][field][1] == null) && !(lager[row][col][field][2] == null)) {
								count++;
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							break;
						} 
					}
				}
			}
			if (count == 0) {	// Falls das Array leer ist, muss es bef�llt werden
				// Leeres Array
				// Personaldaten eingeben
				System.out.println("Das Lager ist leer. Bitte tragen Sie Daten ein.");
				for (int row = 0; row < lager.length; row++) {				// durch jede Reihe des Arrays gehen
					for (int col = 0; col < lager[row].length; col++) {	// durch jede Spalte des Arrays gehen
						for (int field = 0; field < lager[row][col].length; field++) {
							System.out.println("Geben Sie die Daten f�r Regalreihe " + (row+1) + 
									", Regal " + (col+1) + ", Fach " + (field+1) + " ein.");
							for (int entry = 0; entry < lager[row][col][field].length; entry++) {
								if (entry == 0) {									// Eingabe Artikelbezeichnung
									System.out.print("Artikelbezeichnung: ");
									lager[row][col][field][entry] = sc.next();
								} else if (entry == 1) {							// Eingabe Anzahl
									System.out.print("Artikelanzahl (0 - 1000): ");
									lager[row][col][field][entry] = sc.next();
								} else if (entry == 2) {							// Eingabe Preis
									System.out.print("Artikelpreis: ");
									lager[row][col][field][entry] = sc.next();
								}
							}
						}
						
					}
				}
			} else {
				System.out.println("\n------- Lagerverwaltung --------\n");
				System.out.println(" 1  alle Lagereintr�ge eingeben");
				System.out.println(" 2  alle Lagereintr�ge ausgeben");
				System.out.println(" 3  einzelne Datenabfrage");
				System.out.println(" 4  einzelne Daten�nderung");
				System.out.println(" 5  Datensatz suchen");
				System.out.println(" 6  Programm beenden\n");
				System.out.println("--------------------------------");
				System.out.println("Es sind " + count + " von " + size + " F�cher im Lager belegt");
				System.out.print("W�hlen Sie eine Option: ");
				wahl = sc.nextInt();
				System.out.println();
				
				switch (wahl) {
					case 1:
						for (int row = 0; row < lager.length; row++) {				// durch jede Reihe des Arrays gehen
							for (int col = 0; col < lager[row].length; col++) {	// durch jede Spalte des Arrays gehen
								for (int field = 0; field < lager[row][col].length; field++) {
									System.out.println("Geben Sie die Daten f�r Regalreihe " + (row+1) + 
											", Regal " + (col+1) + ", Fach " + (field+1) + " ein.");
									for (int entry = 0; entry < lager[row][col][field].length; entry++) {
										if (entry == 0) {									// Eingabe Artikelbezeichnung
											System.out.print("Artikelbezeichnung: ");
											lager[row][col][field][entry] = sc.next();
										} else if (entry == 1) {							// Eingabe St�ckzahl
											System.out.print("Artikelanzahl (0 - 1000): ");
											tmpInt = 0;
											// Eingabe als ganze Zahl, wird abgefangen wenn:
											// Eingabe keine Zahl ist
											// Eingabe �ber 1000 oder unter 0 ist
											do {
												try {
													tmpInt = sc.nextInt();
												} catch (Exception e) {
													// TODO: handle exception
													System.out.println("Bitte nur eine Zahl zwischen eingeben.");
												} 
											} while ((tmpInt <= 1000) && (tmpInt >= 0));
											 
											lager[row][col][field][entry] = String.valueOf(tmpInt); // Eingabe in String umwandeln
										} else if (entry == 2) {							// Eingabe Preis
											System.out.print("Artikelpreis in Euro (0,01 oder h�her): ");
											tmpDbl = 0;
											// Eingabe als Kommazahl, wird abgefangen wenn:
											// Eingabe keine Zahl ist
											// Eingabe kleiner als 0,01 ist
											do {
												try {
													tmpDbl = sc.nextDouble();
												} catch (Exception e) {
													// TODO: handle exception
													System.out.println("Bitte nur eine Zahl eingeben.");
												}
											} while (tmpDbl < 0.01);
											lager[row][col][field][entry] = String.valueOf(f.format(tmpDbl)) + " �"; // Double formatieren
										}
									}
								}
							}
						}
						break;
					case 2: // Ausgabe der Eintr�ge
						for (int row = 0; row < lager.length; row++) {
							for (int col = 0; col < lager[row].length; col++) {
								for (int field = 0; field < lager[row][col].length; field++) {
									for (int entry = 0; entry < lager[row][col][field].length; entry++) {
										if (entry == 0) {
											System.out.print("Regalreihe " + (row+1) + ", Regal " + (col+1) + ", Fach " + (field+1) + "\n Artikelbezeichnung: " + lager[row][col][field][entry]);
										} else if (entry == 1) {
											System.out.print(", Anzahl: " + lager[row][col][field][entry]);
										} else if (entry == 2) {
											System.out.print(", Preis: " + lager[row][col][field][entry] + "\n");
										}
									}
								}
							}
						}
						break;
					case 3:	// Einzelne Datenabfrage
						System.out.println("Es sind " + (count) + " von " + size + " F�cher im Lager belegt.");
						// Wird wiederholt bis g�ltige Eingabe
						do {
							System.out.print("Geben Sie einen Datensatz ein, den Sie abfragen m�chten: ");
							wahl = sc.nextInt();
						} while (wahl < 1 || wahl > size);
						// for-Schleifen bis zum Datensatz im Array
						// tmp wird nach jedem Fach (field) erh�ht
						// wenn tmp = wahl ist, wird das Fach ausgegeben
						tmpInt = 0;
						for (int row = 0; row < lager.length; row++) {
							for (int col = 0; col < lager[row].length; col++) {
								for (int field = 0; field < lager[row][col].length; field++) {
									tmpInt++;
									if (tmpInt == wahl) {
										for (int entry = 0; entry < lager[row][col][field].length; entry++) {
											if (!(lager[row][col][field][0] == null) && !(lager[row][col][field][1] == null) && !(lager[row][col][field][2] == null)) {
												if (entry == 0) {
													System.out.println("Regalreihe " + (row + 1) + ", Regal " + col
															+ ", Fach " + field);
													System.out.print(
															" Artikelbezeichnung: " + lager[row][col][field][entry]);
												} else if (entry == 1) {
													System.out.print(", Anzahl: " + lager[row][col][field][entry]);
												} else if (entry == 2) {
													System.out.print(", Preis: " + lager[row][col][field][entry] + "\n");
												} 
											} else {
												System.out.println("Der Eintrag in Regalreihe " + (row+1) + ", Regal " + (col+1) + ", Fach " + (field+1) + " ist leer.");
											}
										}
									}
								}
							}
						}
						break;
					case 4:		// Einzelne Daten�nderung
						System.out.println("Es sind " + (count) + " von " + size + " F�cher im Lager belegt.");
						// Wird wiederholt bis g�ltige Eingabe
						do {
							System.out.print("Geben Sie einen Datensatz ein, den Sie �ndern m�chten: ");
							wahl = sc.nextInt();
						} while (wahl < 1 || wahl > size);
						// for-Schleifen bis zum Datensatz im Array
						// tmp wird nach jedem Fach (field) erh�ht
						// wenn tmp = wahl ist, wird das Fach ausgegeben
						tmpInt = 0;
						for (int row = 0; row < lager.length; row++) {
							for (int col = 0; col < lager[row].length; col++) {
								for (int field = 0; field < lager[row][col].length; field++) {
									tmpInt++;
									if (tmpInt == wahl) {
										System.out.println("Geben Sie die neuen Daten f�r Regalreihe " + (row+1) + 
												", Regal " + (col+1) + ", Fach " + (field+1) + " ein.");
										for (int entry = 0; entry < lager[row][col][field].length; entry++) {
											if (entry == 0) {									// Eingabe Artikelbezeichnung
												System.out.print("Artikelbezeichnung: ");
												lager[row][col][field][entry] = sc.next();
											} else if (entry == 1) {							// Eingabe St�ckzahl
												System.out.print("Artikelanzahl (0 - 1000): ");
												tmpInt = 0;
												// Eingabe als ganze Zahl, wird abgefangen wenn:
												// Eingabe keine Zahl ist
												// Eingabe �ber 1000 oder unter 0 ist
												do {
													tmpInt = sc.nextInt(); 
												} while (tmpInt > 1000 || tmpInt < 0);
												 
												lager[row][col][field][entry] = String.valueOf(tmpInt); // Eingabe in String umwandeln
											} else if (entry == 2) {							// Eingabe Preis
												System.out.print("Artikelpreis in Euro (0,01 oder h�her): ");
												tmpDbl = 0;
												// Eingabe als Kommazahl, wird abgefangen wenn:
												// Eingabe keine Zahl ist
												// Eingabe kleiner als 0,01 ist
												do {
													try {
														tmpDbl = sc.nextDouble();
													} catch (Exception e) {
														// TODO: handle exception
														System.out.println("Bitte nur eine Zahl eingeben.");
														sc.nextLine();
													}
												} while (tmpDbl < 0.01);
												lager[row][col][field][entry] = String.valueOf(f.format(tmpDbl)) + " �"; // Double formatieren
											}
										}
									}
								}
							}
						}
						break;
					case 5:		// Datensuche
						// tempor�re Variablen f�r Regalreihe und Regal
						int tmpRR = 0;
						int tmpR = 0;
						do {
							System.out.println("Welchen Artikel m�chten Sie suchen?");
							search = sc.next();		// Eingabe Suchparameter
							for (int row = 0; row < lager.length; row++) {
								for (int col = 0; col < lager[row].length; col++) {
									for (int field = 0; field < lager[row][col].length; field++) {
										// Wenn Artikelbezeichnung im Eintrag gefunden wurde,
										// werden Reihe und Regal gespeichert
										// Fach wird ausgegeben und found auf true gesetzt
										if (lager[row][col][field][0].equalsIgnoreCase(search)) {
											tmpRR = row;
											tmpR = col;
											System.out.println("Datensatz gefunden in:");
											System.out.println(" Regalreihe " + (row+1) + ", Regal " + (col+1) + ", Fach " + (field+1));
											System.out.println("  Artikelbezeichnung: " + lager[row][col][field][0]);
											System.out.println("  Artikelanzahl:      " + lager[row][col][field][1]);
											System.out.println("  Artikelpreis:       " + lager[row][col][field][2]);
											found = true;
										}
									}
								}
							}
							if (!found) {
								System.out.println("Keinen Eintrag gefunden.");
							} else {
								// wenn found = true, dann kann das komplette Regal ausgegeben werden
								System.out.println("\nSoll das gesamte Regal ausgegeben werden? (j)");
								eingabe = sc.next();
								try {
									if (eingabe.equalsIgnoreCase("j")) {
										// Ausgabe Regal mit tempor�ren Variablen
										System.out.println("Eintr�ge f�r Regalreihe " + (tmpRR+1) + ", Regal " + (tmpR+1));
										for (int field = 0; field < lager[tmpRR][tmpR].length; field++) {
											System.out.println(" Fach " + (field+1) + ":");
											System.out.println("  Artikelbezeichnung: " + lager[tmpRR][tmpR][field][0]);
											System.out.println("  Artikelanzahl:      " + lager[tmpRR][tmpR][field][1]);
											System.out.println("  Artikelpreis:       " + lager[tmpRR][tmpR][field][2]);
										}
									}
								} catch (Exception e) {
									// TODO Auto-generated catch block
									System.out.println("Falsche Eingabe");
								}
							}
							// Suche wiederholen
							System.out.println("\nSuche wiederholen? (j)");
							eingabe = sc.next();
							if (!(eingabe.equalsIgnoreCase("j"))) {
								repeat = false;
							}
						} while (repeat);
						repeat = true;
					break;
					case 6:		// Programm beenden
						System.out.println("Programm wird beendet.");
						repeat = false;
						break;
					default:
						System.out.println("keine korrekte Eingabe gefunden.");
				}
				
				
				
			}
		} while (repeat);
		
		sc.close();
		
	}
	// HP Ende
}
// P Ende