package AB01;
import java.util.Scanner;

// P Anfang

/**
 * 
 * Aufgabe5_Suchzahl
 * Suchzahl wird mit der Klasse Zufallszahlen kombiniert
 * In Zufallszahlen wurde eine Methode implementiert, die eine ganze Zufallszahl zur�ckgibt.
 * Die Zufallszahl wird in einem 100-stelligen Array gespeichert.
 * Der Nutzer kann nun eine Zahl zwischen 1 und 50 eingeben, nach der im Array gesucht wird.
 * 
 * @version 1.0 vom 30.01.2020
 * @author matthias.amelung
 * FS94
 *
 */
public class Aufgabe4_Suchzahl {
	
	// HP Anfang
	public static void main(String[] args) throws InterruptedException {
		
		// D Anfang
		Scanner sc = new Scanner(System.in);
		Zufallszahlen zufall = new Zufallszahlen();	// Objekt der Klasse Zufallszahlen
		final int ANZ = 100;
		int arrayZahl[] = new int[ANZ];
		int suchZahl, loc = 0;
		boolean flag = false;
		// D Ende
		
		// Zufallszahlarray erstellen
		for (int i = 0; i < arrayZahl.length; i++) {
			arrayZahl[i] = zufall.genNumber();
		}
		
		// Eingabe der gesuchten Zahl
		do {
			System.out.print("Geben Sie die gesuchte Zahl ein: (1-50) ");
			suchZahl = sc.nextInt();	
		} while (suchZahl < 1 || suchZahl > 50);
	
		// Suche Zahl im Array
		// Wenn Zahl gefunden, bekommt loc den Wert von i (Index)
		// flag wird auf wahr gesetzt und die Schleife verlassen
		for (int i = 0; i < arrayZahl.length; i++) {
			if (arrayZahl[i] == suchZahl) {
				loc = i;
				flag = true;
				break;
			}
			// "Ladebalken" wegen der Optik
			System.out.print(".");
			Thread.sleep(100);
		}
		
		// Ausgabe der gesuchten Zahl und der Index im Array
		if (flag) {
			System.out.println("\nDie Zahl " + suchZahl + " wurde am Index " + loc + " gefunden.");
		} else {
			System.out.println("\nDie Zahl " + suchZahl + " wurde nicht gefunden.");
		}

		// Scanner schlie�en
		sc.close();
	}
	// HP Ende
}
// P Ende