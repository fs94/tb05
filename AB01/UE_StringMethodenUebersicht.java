package AB01;

public class UE_StringMethodenUebersicht{
  public static void main(String[] args){
    final int ANZ;
    char wahl = 'n';
    double dVar = 4.0;
    int iVar = 123,iVar2 = 0;
    do{
      System.out.print( "Anzahl (min 3): ");
      iVar = Tastatur.liesInt();
    }while(iVar < 3);
    ANZ = iVar; //Einmalige Zuweisung der Arraygr��e   
    //Deklaration eines Objekts zur Speicherung von Text
    
    String such = "Test";
    
    String tempText = "Test";
    //Deklaration eines Array zur Speicherung von Texten
    
    String feldText[] = new String[ANZ];
    
    do{
      //Ausgabe aller Arraydaten
      System.out.println("\n\nAusgabe aller Texten direkt nach Deklaration");
      for(int i = 0; i<ANZ; i++){
        
        System.out.println("TextFeld["+i+"]: "+ feldText[i]);//Ausgabe Stringarray
      }
      //Eingabe aller Arraydaten
      System.out.println("\n\nEingabe aller Texte");
      for(int i = 0; i<ANZ; i++){
        System.out.println( "Text f�r Feld["+i+"]: ");
        
        feldText[i] = Tastatur.liesString(); //Eingabe Stringarray
      }
      //Ausgabe aller Arraydaten
      System.out.println("\n\nAusgabe aller Texte");
      for(int i = 0; i<feldText.length; i++){
        
        System.out.println("TextFeld["+i+"]: "+ feldText[i]);//Ausgabe Stringarray
      }
      //Suche nach einem bestimmten Text im Stringarray
      System.out.println("\n\nSuchen nach bestimmten Text");
      System.out.print("Suchtext: ");
      
      such = Tastatur.liesString();
      
      for(int i = 0; i<feldText.length; i++){
        
        if ( such.equals(feldText[i]) == true ) //Im Stringarray in such identisch
          System.out.println( such+" gefunden in feldText["+i+"]");
        
        if ( such.equalsIgnoreCase(feldText[i]) == true ) //Im Stringarray in such identisch
          //Gro�Kleinschreibung ignorieren
          System.out.println( feldText[i]+" gefunden in feldText["+i+"]");
      }
      //Suche nach einem bestimmten TeilText im Stringarray
      System.out.println("\n\nSuchen nach bestimmten TeilText");
      System.out.print("Suchtext: ");

      such = Tastatur.liesString();
      
      for(int i = 0; i<feldText.length; i++){
        
        if ( feldText[i].contains(such) == true ) //Im Stringarray  in such enthalten
          System.out.println( feldText[i]+" gefunden in feldText["+i+"]");
         
        tempText = such.toLowerCase() ;   //komplett kleingeschrieben
        if ( feldText[i].toLowerCase().contains(tempText) == true ){
          //In tempText in such enthalten Gro�Kleinschreibung soll ignoriert werden
          System.out.print(feldText[i]+" gefunden in feldText["+i+"]");
          System.out.println(" - Gro�Kleinschreibung wurde ignoriert");
        }
      }
      
      //Umwandlung eines Doublewertes in einen String
      System.out.println("\n\nReelle Zahl zur Speicherung in feldText[0]: ");
      dVar = Tastatur.liesDouble();
      System.out.println("Ganze Zahl zur Speicherung in feldText[1]: ");
      iVar = Tastatur.liesInt();
      //Umwandlung einer beliebigen Zahl in einen String
      
      feldText[0]= String.valueOf(dVar);
      //Umwandlung nur einer integer Zahl in einen String
      
      feldText[1]= String.valueOf(iVar);
      
      System.out.println("\n\nAusgabe aller Texte");
      for(int i = 0; i<feldText.length; i++){
        System.out.println( "Text Feld["+i+"]: "+feldText[i]);
      }
      iVar = 0;
      dVar = 0;
      for(int i = 0; i < feldText.length; i++){
        try{
          //Anweisungen werden Versucht und beim Fehlversuch kommt catch
          //zur Anwendung
          //Umwandelung von String in Doublewert

          dVar = Double.parseDouble(feldText[i]);
        }catch(NumberFormatException e){  //F�ngt Fehler ab
                                       //Anweisungsblock wenn Fehler gefunden.
          dVar =999;
        }
        if( dVar != 999 ) { //Kein Umwandlugnsfehler ?
          System.out.println(" dVar: " + dVar);
          i = feldText.length;
        } else {
          System.out.println("Fehler bei Umwandlung von Text in Doublezahl");
        }
      }
      System.out.println("Wollen Sie wiederholen(j/n)");
      wahl = Tastatur.liesChar();
    }while(wahl == 'j');
  }
 }

