package AB01;
import java.util.Scanner;

// P Anfang

/**
 * 
 * Aufgabe3
 * Die Temperaturwerte werden vom Nutzer eingeben und
 * anschlie�end ausgegeben.
 * 
 * Teil a, b, c, d, e
 * Men� �ber Switch-Case erzeugt, welches dem Nutzer erlaubt
 * sich einzelne Werte anzuzeigen, einzelne Werte zu �ndern,
 * das komplette Array zu �ndern, das komplette Array anzuzeigen
 * oder die Durchschnittstemperatur zu berechnen.
 * 
 * @version 1.0 am 30.01.2020
 * @author matthias.amelung
 * FS94
 *
 */
public class Aufgabe3 {
	
	// HP Anfang
	public static void main(String[] args) {
		
		// D Anfang
		Scanner sc = new Scanner(System.in);
		float tempWerte[] = new float[10];
		// D Ende
		
		// Eingabe der Temperaturwerte
		// �ber das array iterieren und an jeder Stelle einen Wert eingeben
		for (int i = 0; i < tempWerte.length; i++) {
			System.out.print("Geben Sie den " + (i+1) + ". Temperaturwert ein: ");
			tempWerte[i] = sc.nextFloat();
		}
		
		// Ausgabe der Temperaturwerte
		// mit Formatierung der Ausgabe
		for (int i = 0; i < tempWerte.length; i++) {
			if (i != 0) {
				System.out.print("|  " + (i+1) + ": " + tempWerte[i] + "  ");
			} else {
				System.out.print((i+1) + ": " + tempWerte[i] + "  ");
			}
		}
		
		System.out.println("\n\n\n\n\nAufgabe 3a, 3b, 3c, 3d, 3e\n\n");
		
		// Aufgabe 3a, b, c, d, e
		// D Anfang
		int wahl;	// Variable f�r Switch-Case UND Auswahl des Temperaturwertes
		float summe = 0;
		// D Ende
		
		// Auswahl der Operation
		System.out.println("W�hlen Sie eine Option: ");
		System.out.println(" 1 - Ausgabe eines bestimmtes Temperaturwertes");
		System.out.println(" 2 - Eingabe / Ver�nderung eines Temperaturwertes");
		System.out.println(" 3 - Neue Temperaturwerte eingeben");
		System.out.println(" 4 - Alle Temperaturwerte ausgeben");
		System.out.println(" 5 - Durchschnittstemperatur berechnen");
		wahl = sc.nextInt();
		
		switch (wahl) {
		case 1:	
			do {
				System.out.print("W�hlen Sie einen Temperaturwert: ");
				wahl = sc.nextInt();
			} while (wahl < 1 || wahl > 10);	// sicherstellen, dass nur im Array gearbeitet wird
			System.out.println("Der Wert an Stelle " + (wahl+1) + " ist " + tempWerte[wahl-1]);
			break;
		case 2:
			do {
				System.out.print("W�hlen Sie die Stelle an welcher der Temperaturwert ge�ndert werden soll: ");
				wahl = sc.nextInt();
			} while (wahl < 1 || wahl > 10); 	// sicherstellen, dass nur im Array gearbeitet wird
			System.out.print("neuer Wert: ");
			tempWerte[wahl-1] = sc.nextFloat(); // neuen Wert an der Stelle eingeben (wahl-1, weil nur die Stellen 1 bis 10 eingegeben werden k�nnen)
			break;
		case 3:
			for (int i = 0; i < tempWerte.length; i++) {	// neue Eingabe aller Werte
				System.out.println("Geben Sie den neuen Wert an der Stelle " + (i+1) + "ein: ");
				tempWerte[i] = sc.nextFloat();
			}
			break;
		case 4:
			for (int i = 0; i < tempWerte.length; i++) {	// Ausgabe aller Werte
				if (i != 0) {
					System.out.print("|  " + (i+1) + ": " + tempWerte[i] + "  ");
				} else {
					System.out.print((i+1) + ": " + tempWerte[i] + "  ");
				}
			}
			break;
		case 5:
			// Summe aus allen Temperaturwerten
			for (int i = 0; i < tempWerte.length; i++) {
				summe += tempWerte[i];
			}
			summe /= tempWerte.length;	// summe geteilt durch die Anzahl der Tempraturwerte (10) ergibt die Durchschnittstemperatur
			System.out.println("Die Durchschnittstemperatur ist: " + summe);
			break;
		default:
			System.out.println("falsche Eingabe");
			break;
		}
		
		sc.close();		// Scanner schlie�en
	}
	// HP Ende
}
// P Ende