package AB01;
import java.util.Random;

// P Anfang

/**
 * 
 * Aufgabe6_RNDSumme
 * Das Programm erstellt ein 10000-stelliges Array mit Zufallszahlen zwischen 1 und 6
 * Danach z�hlt das Programm die Menge der 1 und 6 im Array und speichert diese in einem
 * neuen Array. Die H�ufigkeit wird danach ausgegeben.
 * 
 * @version 1.0 vom 30.01.2020
 * @author matthias.amelung
 * FS94
 *
 */
public class Aufgabe6_RNDSumme {
	
	// HP Anfang
	public static void main(String[] args) {

		// D Anfang
		Random zufall = new Random();
		final int ANZ = 10000;
		int arrayZahlen[] = new int[ANZ];
		int arrayFreq[] = new int[2];
		// D Ende
		
		// Array erzeugen
		for (int i = 0; i < arrayZahlen.length; i++) {
			arrayZahlen[i] = 1 + zufall.nextInt(6);
		}
		
		// Z�hlschleife durch das Array
		// z�hlt die Menge von 1 und 6
		for (int i = 0; i < arrayZahlen.length; i++) {
			if (arrayZahlen[i] == 1) {
				arrayFreq[0]++;
			}
			if (arrayZahlen[i] == 6) {
				arrayFreq[1]++;
			}
		}
		
		// Ausgabe
		System.out.println("Die 1 ist " + arrayFreq[0] + "x in der Liste vorhanden");
		System.out.println("Die 6 ist " + arrayFreq[1] + "x in der Liste vorhanden");
		
	}
	// HP Ende
}
// P Ende