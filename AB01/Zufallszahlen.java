package AB01;
import java.util.*;

public class Zufallszahlen {
	public static void main(String[] args) {
		Random zufall = new Random();
		// Erzeugt ein Befehl "zufall" der Zufallszahlen erzeugen kann
		// Der Name des Befehls ist beliebig
		
		int ganzeZahl = 0;
		for (int i = 0; i < 100; i++) {
			ganzeZahl = 1 + zufall.nextInt(50);
			// Erzeugt ganze Zufallszahlen (50) => Werte zwischen 0 - 49
			System.out.println(i + ". ganze Zufallszahl: " + ganzeZahl);
		}
	}
	
	
	// Abwandlung von Zufallszahlen
	// gibt direkt eine Zufallszahl zwischen 1 und 50 zur�ck
	public int genNumber() {
		return 1 + (new Random().nextInt(50));
	}
}
