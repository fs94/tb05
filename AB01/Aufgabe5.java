package AB01;
// P Anfang

/**
 * 
 * Aufgabe5
 * Das Programm erzeugt ein 100-stelliges Array und f�llt es mit Zufallszahlen.
 * Anschlie�end sucht es nach der kleinsten und gr��ten Zahl im Array
 * gibt den Index aus und den Wert der Zahlen.
 * 
 * @version 1.0 vom 30.01.2020
 * @author matthias.amelung
 * FS94
 *
 */
public class Aufgabe5 {

	// HP Anfang
	public static void main(String[] args) {
		
		// D Anfang
		Zufallszahlen zufall = new Zufallszahlen();
		final int ANZ = 100;
		int arrayZahlen[] = new int[ANZ];
		int indexBig = 0, indexSmall = 0;	// index f�r gr��te und kleinste Zahl
		int big = 1, small = 50;			// Wert der gr��ten und kleinsten Zahl
		// D Ende
		
		// Zufallsarray erstellen
		for (int i = 0; i < arrayZahlen.length; i++) {
			arrayZahlen[i] = zufall.genNumber();
		}
		
		// Suche der gr��ten und kleinsten Zahl im Array
		// nimmt die zuerst gefundene Zahl, Duplikate werden nicht ber�cksichtigt
		for (int i = 0; i < arrayZahlen.length; i++) {
			if (arrayZahlen[i] < small) {		// wenn aktuelle Zahl kleiner ist, wird Zahl und index gespeichert
				small = arrayZahlen[i];
				indexSmall = i;
			}
			if (arrayZahlen[i] > big) {			// wenn aktuelle Zahl gr��er ist, wird Zahl und index gespeichert
				big = arrayZahlen[i];
				indexBig = i;
			}
		}
		
		// Ausgabe
		System.out.println("Die gr��te Zahl ist " + big + " an der Stelle " + indexBig);
		System.out.println("Die kleinste Zahl ist " + small + " an der Stelle " + indexSmall);

	}
	// HP Ende
}
// P Ende