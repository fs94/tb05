// P Anfang

/**
 * 
 * Aufgabe1_Verwaltung
 * Personalverwaltungssystem fuer 5 Personen. Jede Person besitzt Vorname, Nachname und Adresse
 * Über ein Menü können Einstellungen an der "Datenbank" vorgenommen werden.
 * Daten koennen vollstaendig neu eingegeben werden oder vollstaendig ausgegeben werden,
 * einzelne Datensaetze koennen geaendert oder ausgegeben werden sowie
 * die Ganzwort-Suche und Stichwort-Suche wird ermoeglicht.
 * 
 * @version 1.0 am 31.01.2020
 * @author matthias.amelung
 * FS94
 * 
 */

package AB02;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Verwaltung {
 
 // HP Anfang
 public static void main(String[] args) {
  
  // D Anfang
  Scanner sc = new Scanner(System.in);      // Scanner
  final int ANZ_P = 5;                      // Anzahl Personen
  final int ANZ_E = 3;                      // Anzahl Eintraege
  int wahl = 0;                             // Switch-Case Menü
  int count = 0;                            // Zaehlvar für Eintraege im Array
  String db[][] = new String[ANZ_P][ANZ_E]; // Datenbank
  String search;                            // Suchparameter
  boolean repeat = true;                    // wdh-Variable
  boolean found = false;                    // Suchparameter gefunden
  // D Ende
  
  ///* DEVELOPMENT DB
  db[0][0] = "Hans";
  db[0][1] = "Meier";
  db[0][2] = "1";
  db[1][0] = "Hans";
  db[1][1] = "Dieter";
  db[1][2] = "2";
  db[2][0] = "Klaus";
  db[2][1] = "Meier";
  db[2][2] = "3";
  db[3][0] = "Klaus";
  db[3][1] = "Dieter";
  db[3][2] = "4";
  db[4][0] = "Hans";
  db[4][1] = "Klaus";
  db[4][2] = "5";
  // ENDE DEVELOPMENT DB */

  // Programm-WDH Beginn
  do {
   count = 0;
   for (int row = 0; row < db.length; row++) {
    if (!(db[row][0] == null && db[row][1] == null && db[row][2] == null)) {
     count++;
    }
   }
   
   if (count == 0) {
    // leeres Array
    // Personaldaten eingeben
    System.out.println("Die Benutzerverwaltung ist leer. Bitte Daten eintragen");
    // durch jede Reihe vom Array gehen
    for (int row = 0; row < db.length; row++) {
     System.out.println("Geben Sie die Daten für den " + (row+1) + ". Datensatz ein.");
     // durch jede Spalte des Array gehen
     for (int col = 0; col < db[row].length; col++) {
      if (col == 0) {                   // Eingabe Vorname
       System.out.print("Vorname: ");
       db[row][col] = sc.next();
      } else if (col == 1) {            // Eingabe Nachname
       System.out.print("Nachname: ");
       db[row][col] = sc.next();
      } else if (col == 2) {            // Eingabe Adresse
       System.out.print("Adresse: ");
       db[row][col] = sc.next();
      }
     }
    }
   } else {
    System.out.println("\n----- Personenverwaltung ------\n");
	System.out.println(" 1  alle Personaldaten eingeben");
	System.out.println(" 2  alle Personaldaten ausgeben");
	System.out.println(" 3  einzelne Datenabfrage");
	System.out.println(" 4  einzelne Datenänderung");
	System.out.println(" 5  Datensatz suchen");
	System.out.println(" 6  Programm beenden\n");
    System.out.println("-------------------------------");
    
    if (count > 1) {
     System.out.println("Es sind " + count + " von " + ANZ_P + " Einträgen in der Datenbank.");
    } else {
     System.out.println("Es ist " + count + " von " + ANZ_P + " Einträgen in der Datenbank.");
    }

    do {
     System.out.println("Wählen Sie eine Option: ");
     try {
      wahl = sc.nextInt();
     } catch (Exception e) {
      System.out.println("Bitte nur Zahlen eingeben.");
      sc.nextLine();
     }
    } while (wahl < 1 || wahl > 6);
    System.out.println();

    switch (wahl) {
     case 1:        // Personaldaten eingeben
      for (int row = 0; row < db.length; row++) {	    // durch jede Reihe des Arrays gehen
	   System.out.println("Geben Sie die Daten für den " + (row+1) + ". Datensatz ein.");
	   for (int col = 0; col < db[row].length; col++) {	// durch jede Spalte des Arrays gehen
	    if (col == 0) {									// Eingabe Vorname
		 System.out.print("Vorname: ");
		 db[row][col] = sc.next();
	    } else if (col == 1) {							// Eingabe Nachname
		 System.out.print("Nachname: ");
		 db[row][col] = sc.next();
	    } else if (col == 2) {							// Eingabe Adresse
		 System.out.print("Adresse: ");
		 db[row][col] = sc.next();
	    }
	   }
	  }
      break;
     case 2:        // Personaldaten ausgeben
      for (int row = 0; row < db.length; row++) {	    // geht durch jede Reihe des Arrays
	   System.out.print((row+1) + ". Datensatz:");
	   for (int col = 0; col < db[row].length; col++) {	// geht durch jede Spalte des Arrays
		if (!(db[row][0] == null)) {
		 if (col != (db[row].length-1)) {
		  System.out.print(" " + db[row][col]);
		 } else {
		  System.out.print(", " + db[row][col] + "\n");
		 }
		} else {
		 System.out.print("leerer Datensatz\n");
		}
	   }
	  }
      break;
     case 3:            // Einzelabfrage
      if (count > 1) {
       System.out.println("Es sind " + count + " von " + ANZ_P + " Einträgen in der Datenbank");
      } else {
       System.out.println("Es ist " + count + " von " + ANZ_P + " Einträgen in der Datenbank");
      }
      do {
       System.out.print("Geben Sie an welchen Datensatz sie abfragen möchten: ");
       wahl = sc.nextInt();
      } while (wahl < 1 || wahl > ANZ_P);
                   
      System.out.print(wahl + ". Datensatz:");
      for (int col = 0; col < db[wahl-1].length; col++) {		// Gibt alle Daten in der Reihe (Datensatz) aus
       if (db[wahl-1][col] == null) {
        System.out.print("leerer Datensatz\n");
       } else {
        if (col != (db[wahl-1].length-1)) {
         System.out.print(" " + db[wahl-1][col]);
        } else {
         System.out.print(", " + db[wahl-1][col] + "\n");
        }
       }
      }
      break;
     case 4:		// Einzelaenderung
      do {
       System.out.println("Sie können zwischen Datensatz 1 - 5 auswählen.");
       System.out.print("Geben Sie an welchen Datensatz sie ändern möchten: ");
       try {
        wahl = sc.nextInt();
       } catch (InputMismatchException e) {
        System.out.println("Bitte nur Zahlen eingeben");
        sc.nextLine();
       }
      } while (wahl < 1 || wahl > 5);		// nur Datensätze zwischen 1 und 5
                 
      System.out.println("\nGeben Sie die Daten für den " + (wahl) + ". Datensatz ein.");
      for (int col = 0; col < db[wahl-1].length; col++) {	// durch jede Spalte des Arrays gehen
       if (col == 0) {						// Eingabe Vorname
        System.out.print("Vorname: ");
        db[wahl-1][col] = sc.next();
       } else if (col == 1) {				// Eingabe Nachname
        System.out.print("Nachname: ");
        db[wahl-1][col] = sc.next();
       } else if (col == 2) {				// Eingabe Adresse
        System.out.print("Adresse: ");
        db[wahl-1][col] = sc.next();
       }
      }
      break;
     case 5:        // Suche
      do {  // Eingabe Suchparameter
       found = false;
       System.out.print("Geben Sie den Suchparameter ein: ");
       search = sc.next();
       for (int row = 0; row < db.length; row++) {						// Durch jede Reihe des Arrays gehen
        for (int col = 0; col < db[row].length; col++) {			// Durch jede Spalte der Reihe des Arrays gehen
         if (!(db[row][col] == null)) {
          if (db[row][col].compareToIgnoreCase(search) == 0) {	// Vergleicht gesamten String, nicht casesensitiv
                       
           // Ausgabe des Datensatzes
           System.out.println("Der Parameter \"" + search + "\" wurde gefunden in:");
           System.out.println((row+1) + ". Datensatz, Feld " + (col+1));
           System.out.print("Inhalt:");
           for (int col2 = 0; col2 < db[row].length; col2++) {
            if (col2 != (db[row].length-1)) {
             System.out.print(" " + db[row][col2]);
            } else {
             System.out.print(", " + db[row][col2] + "\n");
            }
           }
          found = true;
          } else {
           if (db[row][col].toLowerCase().contains(search.toLowerCase())) { // schaut ob Zeichenkette im Datensatz enthalten ist
            // Ausgabe des Datensatzes
            System.out.println("Der Parameter \"" + search + "\" wurde gefunden in:");
            System.out.print((row+1) + ". Datensatz: ");
            for (int col2 = 0; col2 < db[row].length; col2++) {
             if (col2 != (db[row].length-1)) {
              System.out.print(" " + db[row][col2]);
             } else {
              System.out.print(", " + db[row][col2] + "\n");
             }
            }
            found = true;
           }
          }
         }
        }
       }
       // Ausgabe für den Nutzer
       if (!found) {
        System.out.println("Der Parameter \"" + search + "\" wurde nicht gefunden.");
       } 
       do {
        found = true;
        System.out.print("\nSuche wiederholen? (j) ");
        try {
         search = String.valueOf(sc.next(".").toLowerCase().charAt(0));
         found = false;;
        } catch (InputMismatchException e) {
         System.out.println("Bitte nur j eingeben um zu wiederholen.");
         sc.nextLine();
        }	
       } while (found);
      } while (search.equals("j"));
      break;
     case 6:
      repeat = false;
      break;
     default:
      System.out.println("\nKeine Korrekte Eingabe.\nBitte geben Sie einen der Menüpunkte ein.\n");
      break;
    }
   }
  } while (repeat);
  // Programm-WDH Ende
  
  // Scanner schließen
  sc.close();
 }
 // HP Ende
}
// P Ende